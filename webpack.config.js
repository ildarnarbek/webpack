const SvgStorePlugin = require('external-svg-sprite-loader');
const webpack = require('webpack');
const SitemapPlugin = require('./plugins/sitemap/index');
const fs = require('fs');
const path = require('path');
const glob = require('glob');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MediaQueryPlugin = require('./plugins/media-query-plugin/src/index');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const HashPlugin = require('hash-webpack-plugin');

const pageSetting = require('./pageSetting');

const plugins = [
  new CleanWebpackPlugin(),
  new MiniCssExtractPlugin({
    filename: "./assets/styles/[name]-[hash].css",
    chunkFilename: "./assets/styles/[name]-[hash].css"
  }),
  new CopyWebpackPlugin([
    {
      from: './source/static',
      to: './',
      ignore: ['*.md']
    }
  ]),
  new SvgStorePlugin({
    sprite: {
      startX: 10,
      startY: 10,
      deltaX: 20,
      deltaY: 20,
      iconHeight: 20,
    },
    prefix: 'usage',
    suffix: ''
  })
];

module.exports = (env = {}, argv) => {
  const dirEntry = './source/entity/';
  let pages = [];
  if (env.page) {
    pages = glob.sync(__dirname + '/source/pages/'+env.page+'.pug');
  } else {
    pages = glob.sync(__dirname + '/source/pages/*.pug');
  }
  pages.map(function (file) {
    let base = path.basename(file, '.pug');
    let chunk = (pageSetting[base] ? pageSetting[base] : base).split('/');

    plugins.push(new HtmlWebpackPlugin({
      filename: './' + base + '.html',
      chunks: chunk.concat(['vendor', 'common']),
      template: './source/pages/' + base + '.pug',
      inject: false
    }));
  });

  if (env && env.develop === 'true') {
    plugins.push(new webpack.DefinePlugin({
      IS_DEV: true,
    }));
  } else {
    plugins.push(new SitemapPlugin({}));
    plugins.push(new HashPlugin({
      path: './dist/',
      fileName: 'hash.txt'
    }));
    plugins.push(new webpack.DefinePlugin({
      IS_DEV: false,
    }));
    new MediaQueryPlugin({
      include: true,
      queries: {
        '(max-width: 767px)': 'mobile',
        '(min-width: 1024px)': 'tablet',
        '(min-width: 1280px)': 'desktop',
        '(min-width: 1440px)': 'desktop',
      }
    })
  }
  return {
    entry: fs.readdirSync(dirEntry).reduce((summ, item) => {
        if (env.page) {
          let chunk = (pageSetting[env.page] ? pageSetting[env.page] : env.page).split('/');
          if (['common'].concat(chunk).indexOf(item) !== -1) {
            summ[item] = `${dirEntry}${item}/${item}.js`;
          }
        } else {
          summ[item] = `${dirEntry}${item}/${item}.js`;
        }
      return summ;
    }, {}),
    output: {
      filename: './assets/scripts/[name]-[hash].bundle.js',
      chunkFilename: './assets/scripts/chunk/[id]-[hash].chunk.js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/'
    },
    plugins,
    module: {
      rules: [
        {
          test: /\.json$/,
          use: [
            {
              loader: path.resolve('./loaders/split-json-loader.js'),
              options: {
                dir: path.resolve('./source/data')
              }
            }
          ]
        },
        (env.develop === 'true') ? {
          test: /\.scss$/,
          use: [
            'style-loader',
            "css-loader",
            {
              loader: "sass-loader",
              options: {
                includePaths: [
                  path.resolve(__dirname,'source/base/styles')
                ]
              }
            },
            {
              loader: path.resolve('./loaders/scss-import-base.js'),
              options: {
                baseStyles: path.resolve('source/base/styles'),
                name: '[name].[ext]',
              }
            },
          ]
        } : {
          test: /\.scss$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                publicPath: './../../'
              }
            },
            "css-loader",
            MediaQueryPlugin.loader,
            {
              loader: "sass-loader",
              options: {
                includePaths: [
                  path.resolve(__dirname,'source/base/styles')
                ]
              }
            },
            {
              loader: path.resolve('./loaders/scss-import-base.js'),
              options: {
                baseStyles: path.resolve('source/base/styles'),
                name: '[name].[ext]',
              }
            },
          ]
        },
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: [
            // "cache-loader",
            {
              loader: 'babel-loader',
              options: {
                "presets": ["@babel/preset-env"],
                "plugins": ["@babel/plugin-syntax-dynamic-import", "@babel/plugin-proposal-class-properties"]
              }
            }]
        },
        {
          loader: SvgStorePlugin.loader,
          test: /\.svg$/,
          exclude: [path.resolve(__dirname, 'source/assets/images')],
          options: {
            iconName: '[name]-usage',
            name: './assets/sprite.svg',
          },
        },
        {
          test: /\.(png|jpg|webp|svg?)(\?.+)?$/,
          exclude: [path.resolve(__dirname, 'source/assets/svg')],
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'assets/images/[name].[ext]',
              }
            }
          ]
        },
        {
          test: /\.(ttf|eot|woff|woff2)$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "assets/fonts/[name].[ext]",
              },
            }
          ],
        },
        {
          test: /\.pug$/,
          include: /source[\/,\\]pages/,
          use: [
            {
              loader: "pug-loader",
              options: {
                pretty: true,
                root: path.resolve(__dirname, './source/')
              }
            }
          ]
        },
        {
          test: /\.pug$/,
          exclude: /source[\/,\\]pages/,
          use: [
            {
              loader: "storypug/lib/webpack-loader.js",
              options: {
                pretty: true,
                root: path.resolve(__dirname, './source/')
              }
            }
          ]
        }
      ]
    },
    resolve: {
      extensions: [".js"],
      alias: {
        "TweenMax": 'gsap/src/uncompressed/TweenMax',
        "TimelineMax": 'gsap/src/uncompressed/TimelineMax',
        "TweenLite": 'gsap/src/uncompressed/TweenLite',
        "TimelineLite": 'gsap/src/uncompressed/TimelineLite',
        "ScrollMagic": 'scrollmagic/scrollmagic/uncompressed/ScrollMagic',
        "animation.gsap": 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap',
        "debug.addIndicators": 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators',
        "@": path.resolve(__dirname,'./'),
        "@source": path.resolve(__dirname,'./source'),
        "@assets": path.resolve(__dirname,'./source/assets'),
        "@blocks": path.resolve(__dirname,'./source/blocks'),
        "@core": path.resolve(__dirname,'./source/core'),
        "@base": path.resolve(__dirname,'./source/base'),
      },
    },
    optimization: (env.develop === 'true') ? {
      removeAvailableModules: false,
      removeEmptyChunks: false,
      splitChunks: false,
    } : {
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all',
          }
        }
      },
      minimizer: [
        new TerserPlugin(),
        new OptimizeCSSAssetsPlugin({
          cssProcessor: require('cssnano'),
          cssProcessorOptions: {
            zindex: false,
            reduceIdents: false,
            discardUnused: false
          },
        }),
      ],
    },
    devServer: {
      host: '0.0.0.0',
      watchContentBase: true,
      port: 80,
      open: true,
      disableHostCheck: true,
      noInfo: true,
      compress: true,
      hot: false,
      stats: 'minimal',
      publicPath: "/",
      before(app){
          var bodyParser = require('body-parser');
          app.use(bodyParser.json());
      }
    }
  };
};
