import { renderer } from 'storypug';

export const loadAssets = () => {
  const svg = require.context(
    '!!raw-loader!@assets/',
    true, // Load files recursively. Pass false to skip recursion.
    /^\.\/.*\.(svg)$/, // Match files ending with .md.
  );
  const images = require.context(
    '@assets/',
    true, // Load files recursively. Pass false to skip recursion.
    /^\.\/.*\.(jpg|png)$/, // Match files ending with .md.
  );
  return (file) => {
    let result = false;
    if (svg.keys().indexOf(file) !== -1) {
      result = svg(file).default;
    } else {
      result = images(file);
    }
    return result;
  };
};
const { html } = renderer({ loadAssets: loadAssets() });

export default html;
